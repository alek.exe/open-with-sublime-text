# Open With Sublime
##### Very simple batch program **FOR WINDOWS ONLY** that adds "Open with Sublime" to your right click menu.
After starting this program, you will have same option as in Notepad++, but with Sublime Text 3.<br>
Very simple, very fast and very effective. Sublime Text 3 can be installed anywhere, program adds it relative to itself.


### How to use it?
Download it by clicking on cloud icon under this projects name, add ows.exe to Sublimes **root folder**, start ows.exe and you are done.

### Is it safe?
Yes, but maybe some anti-viruses will notify you because it adds registry key entries.
If you want to be sure, check branch of this project named "source" and check source yourself.


### Can I change text?
I like you. Check "source" branch and edit code yourself, it is written in batch and it is very simple to understand.

### Can I remove this feature, and how if possible?
You can. Press Windows key and R at the same time and type regedit.
Navigate to following key: HKEY_CLASSES_ROOT\*\shell and remove folder "Open with Sublime Text"

### Preview
![Program](https://gitlab.com/alek.exe/open-with-sublime-text/raw/master/!git/preview.PNG)
![Right click](https://gitlab.com/alek.exe/open-with-sublime-text/raw/master/!git/preview.gif)